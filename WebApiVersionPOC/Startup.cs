using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using System;
using System.Linq;
using WebApiVersionPOC.Controllers;

namespace WebApiVersionPOC
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            //Default versiong implementation
            services.AddApiVersioning(x =>
            {
                x.DefaultApiVersion = new ApiVersion(1, 0);
                x.AssumeDefaultVersionWhenUnspecified = true;
                x.ReportApiVersions = true;
                x.ApiVersionReader = new HeaderApiVersionReader("x-api-version");
                x.Conventions.Controller<EmployeeV1Controller>().HasApiVersion(new ApiVersion(1, 0));
                x.Conventions.Controller<EmployeeV2Controller>().HasApiVersion(new ApiVersion(2, 0));
                x.Conventions.Controller<WeatherForecastController>().HasApiVersion(new ApiVersion(1, 0));
            });

            //swagger document creation for every version
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1.0",
                    Title = "API version v1",
                    Description = "sample v1"
                });
                c.SwaggerDoc("v2", new OpenApiInfo
                {
                    Version = "v2.0",
                    Title = "API version v2",
                    Description = "sample v2"
                });

                c.ResolveConflictingActions(a => a.First());

                c.OperationFilter<SwaggerParameterFilters>();
                c.DocumentFilter<SwaggerVersionMapping>();
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            //swagger starting page with version wise
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint($"/swagger/v1/swagger.json", "API v1");
                c.SwaggerEndpoint($"/swagger/v2/swagger.json", "API v2");
            });
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }

    
    
}
