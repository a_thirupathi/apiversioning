﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiVersionPOC
{
    public class Employee
    {
        public string UserName { set; get; }
        public string Designation { set; get; }
        public int Age { set; get; }
        public bool Active { set; get; }
        //assumed added field in Version 2 
        public string EMail { set; get; }
        //added new class and consumed into new version
        public List<Department> departmentsList { set; get; }
    }


    public class Department
    {
        public int Id { set; get; }
        public string DepartmentName { set; get; }

    }
}
