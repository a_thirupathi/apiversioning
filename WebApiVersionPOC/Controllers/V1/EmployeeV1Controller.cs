﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebApiVersionPOC
{
    [Route("api/v{v:apiVersion}/[controller]/")]
    [ApiController]
    [ApiExplorerSettings(GroupName = "v1")]
    public class EmployeeV1Controller : ControllerBase
    {
        // GET: api/<EmployeeV1Controller>
        [HttpGet]
        public IEnumerable<Employee> Get()
        {
            List<Employee> emp = new List<Employee>();
            emp.Add(new Employee { UserName = "Thirupathi", Age = 27, Designation = "Assosiate", Active = true });
            emp.Add(new Employee { UserName = "Punam", Age = 28, Designation = "Assosiate", Active = true });
            emp.Add(new Employee { UserName = "Priya", Age = 29, Designation = "Assosiate", Active = false });
            return emp;
        }

        //[HttpGet]
        //[MapToApiVersion("2.0")]
        //public IEnumerable<Employee> Get1()
        //{
        //    List<Employee> emp = new List<Employee>();
        //        emp.Add(new Employee { UserName = "Thirupathi", Age = 27, Designation = "Assosiate", Active = true });
        //            emp.Add(new Employee { UserName = "Punam", Age = 28, Designation = "Assosiate", Active = true });
        //emp.Add(new Employee { UserName = "Priya", Age = 29, Designation = "Assosiate", Active = false });
        //return emp;
        //}

    }
}
