﻿using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiVersionPOC
{
    public class SwaggerParameterFilters : IOperationFilter
    {
        public void Apply(OpenApiOperation operation, OperationFilterContext context)
        {
            if (operation.Parameters.Count > 0)
            {
                var versionparameter = operation.Parameters.Single(p => p.Name == "v");
                operation.Parameters.Remove(versionparameter);
            }
        }
    }

    public class SwaggerVersionMapping : IDocumentFilter
    {
        public void Apply(OpenApiDocument swaggerDoc, DocumentFilterContext context)
        {
            var paths = swaggerDoc.Paths;
            swaggerDoc.Paths = new OpenApiPaths();
            foreach (var item in paths)
            {
                var key = item.Key.Replace("v{v}", swaggerDoc.Info.Version);
                var value = item.Value;
                swaggerDoc.Paths.Add(key, value);
            }
        }
    }
}
